////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>
#include <nir/reflection_.h>
#include <fstream>
#include <filesystem>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wmultiple-inheritance"
#endif

class entity {
};

class transformable {
};

class player final : public entity, public transformable {
	public:
	player() noexcept :
		entity{},
		transformable{},
		pos_x{0},
		pos_y{0}
	{
	}

	player(const player& src) noexcept :
		entity{src},
		transformable{src},
		pos_x{src.pos_x},
		pos_y{src.pos_y}
	{
	}

	player(player&& src) noexcept :
		entity{std::move(src)},
		transformable{std::move(src)},
		pos_x{src.pos_x},
		pos_y{src.pos_y}
	{
		src.pos_x = src.pos_y = 0;
	}

	player(const int pos_x, const int pos_y) noexcept :
		entity{},
		transformable{},
		pos_x{pos_x},
		pos_y{pos_y}
	{
	}

	~player()
	{
	}

	player& operator =(player rhs) noexcept
	{
		entity::operator =(std::move(rhs));
		transformable::operator =(std::move(rhs));

		pos_x = rhs.pos_x;
		pos_y = rhs.pos_y;

		rhs.pos_x = rhs.pos_y = 0;

		return *this;
	}

	int get_pos_x() const noexcept
	{
		return pos_x;
	}

	void set_pos_x(const int pos_x) noexcept
	{
		this->pos_x = pos_x;
	}

	int get_pos_y() const noexcept
	{
		return pos_y;
	}

	void set_pos_y(const int pos_y) noexcept
	{
		this->pos_y = pos_y;
	}

	void move(const int x, const int y) noexcept
	{
		pos_x += x;
		pos_y += y;
	}

	private:
	int pos_x,
	    pos_y;
};

NIR_REFLECT(player)<nir::usr::base<entity>{},
                    nir::usr::base<transformable>{},
                    nir::usr::member_function{"move", &player::move},
                    nir::usr::member_object{"pos_x", &player::pos_x},
                    nir::usr::member_object{"pos_y", &player::pos_y}>;

bool test_base_names(const nir::reflection_<player>& player_reflection) noexcept
{
	std::string base_names;
	for (const bad::type& player_base_type : player_reflection.get_base_types()) {
		if (!base_names.empty())
			base_names += ", ";
		base_names += player_base_type.name;
	}

	return base_names ==
#if defined(__GNUC__)
		"entity, transformable"
#elif defined(_MSC_VER)
		"class entity, class transformable"
#endif
	;
}

constinit const char* const PLAYER_CONFIG_PATH{"player.conf"};

void save_player(const player& player) noexcept
{
	std::ofstream output{PLAYER_CONFIG_PATH};
	output << "pos_x " << player.get_pos_x() << '\n'
	       << "pos_y " << player.get_pos_y() << '\n';
}

player load_player(const nir::reflection_<player>& player_reflection) noexcept
{
	player player;

	std::ifstream input{PLAYER_CONFIG_PATH};
	std::string key;
	int value;
	input >> key;
	input >> value;
	player_reflection.get_member_object(key)->set_value(&player, std::make_any<int>(value));
	input >> key;
	input >> value;
	player_reflection.get_member_object(key)->set_value(&player, std::make_any<int>(value));

	return player;
}

int main()
{
	bool result = true;

	const nir::reflection_<player> player_reflection;

	result &= test_base_names(player_reflection);

	player player;
	player.set_pos_x(2);
	player.set_pos_y(2);
	const nir::member_function* const player_move_member_function = player_reflection.get_member_function("move");
	player_move_member_function->call(&player, {1, 2});
	const std::vector<bad::type> move_param_types = player_move_member_function->get_param_types();
	result &= player_move_member_function->get_return_type().name == "void" &&
	          move_param_types[0].name == "int" &&
	          move_param_types[1].name == "int";

	save_player(player);

	player = load_player(player_reflection);
	result &= player.get_pos_x() == 3 && player.get_pos_y() == 4;

	std::filesystem::remove(PLAYER_CONFIG_PATH);

	return !result;
}
