////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_lexer.h>
#include <utility>

json_lexer::json_lexer(const char* const input) noexcept :
	reader{input}
{
}

json_lexer::json_lexer(const json_lexer& src) noexcept :
	reader{src}
{
}

json_lexer::json_lexer(json_lexer&& src) noexcept :
	reader{std::move(src)}
{
}

json_lexer::~json_lexer()
{
}

json_lexer& json_lexer::operator =(json_lexer rhs) noexcept
{
	reader::operator =(std::move(rhs));

	return *this;
}

std::string json_lexer::next_token(const read_mode read_mode) noexcept
{
	std::string token;

	const char* input = get_input();
	{
		enum class state {
			CONSUME_WHITESPACE,
			PARSE,
			PARSE_STRING,
			FINISH
		} state{state::CONSUME_WHITESPACE};
		bool input_inc;
		const auto is_notation = [] (const char c)
		                         {
		                         	return c == '{' || c == '}' || c == '[' || c == ']' || c == ',' || c == ':';
		                         };
		for (char c = *input; state != state::FINISH && c != '\0'; c = *(input += input_inc)) {
			input_inc = true;
			switch (state) {
				case state::CONSUME_WHITESPACE:
				if (!std::isspace(c)) {
					token += c;
					if (is_notation(c))
						state = state::FINISH;
					else if (c == '"')
						state = state::PARSE_STRING;
					else
						state = state::PARSE;
				}
				break;

				case state::PARSE:
				if (std::isspace(c) || is_notation(c)) {
					state = state::FINISH;
					input_inc = false;
				}
				else
					token += c;
				break;

				case state::PARSE_STRING:
				token += c;
				if (c == '"')
					state = state::FINISH;
				break;

				default:
				;
			}
		}
	}
	if (read_mode == read_mode::STEP)
		set_input(input);

	return token;
}
