////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <nir/member_object.h>
#include <utility>

namespace nir
{

member_object::member_object(const std::string_view& name) noexcept :
	member{name}
{
}

member_object::member_object(const member_object& src) noexcept :
	member{src}
{
}

member_object::member_object(member_object&& src) noexcept :
	member{std::move(src)}
{
}

member_object::~member_object()
{
}

member_object& member_object::operator =(member_object rhs) noexcept
{
	member::operator =(std::move(rhs));

	return *this;
}

bad::type member_object::get_type() const noexcept
{
	return {};
}

std::any member_object::get_value([[maybe_unused]] const void* const instance) const noexcept
{
	return {};
}

void member_object::set_value([[maybe_unused]] void* const instance,
                              [[maybe_unused]] const std::any& value) const noexcept
{
}

}// namespace nir
