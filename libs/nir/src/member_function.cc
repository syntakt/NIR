////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <nir/member_function.h>
#include <utility>

namespace nir
{

member_function::member_function(const std::string_view& name) noexcept :
	member{name}
{
}

member_function::member_function(const member_function& src) noexcept :
	member{src}
{
}

member_function::member_function(member_function&& src) noexcept :
	member{std::move(src)}
{
}

member_function::~member_function()
{
}

member_function& member_function::operator =(member_function rhs) noexcept
{
	member::operator =(std::move(rhs));

	return *this;
}

bad::type member_function::get_return_type() const noexcept
{
	return {};
}

std::vector<bad::type> member_function::get_param_types() const noexcept
{
	return {};
}

std::any member_function::call([[maybe_unused]] void* const instance,
                               [[maybe_unused]] const std::vector<std::any>& args) const noexcept
{
	return {};
}

}// namespace nir
