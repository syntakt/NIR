////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_REFLECTOR_H
#define NIR_REFLECTOR_H

#include <bad/macro.h>
#include "reflectives.h"

#define NIR_REFLECT(...)\
NIR_REFLECT_TYPE(BAD_ARG(__VA_ARGS__))

#define NIR_REFLECT_TYPE(type)\
template struct ::nir::reflector<type>::registry

namespace nir
{

/// \brief Exposes reflective interface for metadata registration.
///
/// Reflector is the kind of class through which user of NIR specifies which metadata are assigned to reflected types.
/// It works by specializing nir::reflector for the class intended for introspection. The following code snippet
/// illustrates their usage (the NIR_REFLECT macro is just a template specialization convenience):
/// \code{.cpp}
/// NIR_REFLECT(my_class)<nir::usr::base<my_class_base>{},
///                       nir::usr::member_function{"set_value", &my_class::set_value},
///                       nir::usr::member_object{"value", &my_class::value}>;
/// \endcode
template <bad::structural t>
struct reflector final {
	template <auto... ARGS>
	struct registry final {
		friend constexpr auto nir_get_bases(const reflector&) noexcept
		{
			return _detail::_filter_bases(ARGS...);
		}

		friend constexpr auto nir_get_member_functions(const reflector&) noexcept
		{
			return _detail::_filter_member_functions(ARGS...);
		}

		friend constexpr auto nir_get_member_objects(const reflector&) noexcept
		{
			return _detail::_filter_member_objects(ARGS...);
		}
	};

	friend constexpr auto nir_get_bases(const reflector&) noexcept;

	friend constexpr auto nir_get_member_functions(const reflector&) noexcept;

	friend constexpr auto nir_get_member_objects(const reflector&) noexcept;

	static constexpr auto create_base_type_list() noexcept;

	static constexpr auto create_member_function_tuple() noexcept;

	static constexpr auto create_member_object_tuple() noexcept;

	private:
	template <bad::type_variadic<bad::type_list> tl>
	static constexpr auto cat_base_type_lists() noexcept;

	template <bad::type_variadic<bad::type_list> tl>
	static constexpr auto cat_base_member_function_tuples() noexcept;

	template <bad::type_variadic<bad::type_list> tl>
	static constexpr auto cat_base_member_object_tuples() noexcept;
};

}// namespace nir

#include "_detail/reflector.inl"

#endif
