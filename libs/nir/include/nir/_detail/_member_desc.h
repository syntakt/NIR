////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR__DETAIL__MEMBER_DESC_H
#define NIR__DETAIL__MEMBER_DESC_H

#include <bad/type/concepts.h>
#include <bad/type/type_ops.h>
#include <nir/member_object_interface.h>

namespace nir::_detail
{

template <std::size_t NAME_SIZE, typename t>
struct _member_desc final {
	constexpr _member_desc(const char (&name)[NAME_SIZE], t def) noexcept;

	char name[NAME_SIZE];

	t def;
};


template <typename t>
concept _member_descriptive = requires (t* const * const x) { [] <std::size_t NAME_SIZE, typename ptr_type> (_member_desc<NAME_SIZE, ptr_type>* const * const) noexcept {}(x); };

template <typename t>
concept _member_object_interfacing = requires (t* const * const x)
{
	[] <bad::member_object_pointing p,
	    bad::invocable_r<typename bad::member_pointer_traits<p>::value_type, const typename bad::member_pointer_traits<p>::class_type&> g,
	    std::invocable<typename bad::member_pointer_traits<p>::class_type&, const typename bad::member_pointer_traits<p>::value_type&> s>
	   (member_object_interface<p, g, s>* const * const) noexcept {}(x);
};

template <typename t>
concept _member_object_defining = bad::member_object_pointing<t> || _member_object_interfacing<t>;

}// namespace nir::_detail

#endif
