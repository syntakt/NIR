////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/container/tuple_iterator.h>

namespace nir
{

inline constexpr bad::type reflection::get_type() const noexcept
{
	return type;
}

inline constexpr const std::vector<bad::type>& reflection::get_base_types() const noexcept
{
	return base_types;
}

inline const bad::flat_set<const member_function*>& reflection::get_member_functions() const noexcept
{
	return member_functions;
}

inline const bad::flat_set<const member_object*>& reflection::get_member_objects() const noexcept
{
	return member_objects;
}

inline void reflection::populate_member_functions(bad::type_variadic<std::tuple> auto& member_function_tuple) noexcept
{
	populate(member_function_tuple, member_functions);
}

inline void reflection::populate_member_objects(bad::type_variadic<std::tuple> auto& member_object_tuple) noexcept
{
	populate(member_object_tuple, member_objects);
}

template <std::derived_from<member> t>
void reflection::populate(bad::type_variadic<std::tuple> auto& tuple, bad::flat_set<const t*>& members) noexcept
{
	bad::tuple_iterator{tuple}(
		[&members] (const auto& member)
		{
			members.insert(&member);
			return false;
		}
	);
}

}// namespace nir
