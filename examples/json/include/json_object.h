////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef JSON_OBJECT_H
#define JSON_OBJECT_H

class json_writer;
class json_reader;

class json_object {
	public:
	json_object() noexcept;

	json_object(const json_object& src) noexcept;

	json_object(json_object&& src) noexcept;

	virtual ~json_object();

	json_object& operator =(json_object rhs) noexcept;

	virtual void serialize(json_writer& json_writer) const noexcept;

	virtual void deserialize(json_reader& json_reader) noexcept;
};

#endif
