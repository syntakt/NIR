////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>

namespace nir
{

template <bad::member_object_pointing t>
member_object_<t>::member_object_(const std::string_view& name, const t ptr) noexcept :
	member_object{name},
	ptr{ptr},
	getter{[ptr] (const class_type& instance) noexcept { return instance.*ptr; }},
	setter{[ptr] (class_type& instance, const value_type& value) noexcept { instance.*ptr = value; }}
{
}

template <bad::member_object_pointing t>
template <bad::invocable_r<typename member_object_<t>::value_type, const typename member_object_<t>::class_type&> g,
          std::invocable<typename member_object_<t>::class_type&, const typename member_object_<t>::value_type&> s>
member_object_<t>::member_object_(const std::string_view& name, const member_object_interface<t, g, s>& interface) noexcept :
	member_object{name},
	ptr{interface.ptr},
	getter{interface.getter},
	setter{interface.setter}
{
}

template <bad::member_object_pointing t>
member_object_<t>::member_object_(const member_object_& src) noexcept :
	member_object{src},
	ptr{src.ptr},
	getter{src.getter},
	setter{src.setter}
{
}

template <bad::member_object_pointing t>
member_object_<t>::member_object_(member_object_&& src) noexcept :
	member_object{std::move(src)},
	ptr{src.ptr},
	getter{std::move(src.getter)},
	setter{std::move(src.setter)}
{
	src.ptr = nullptr;
}

template <bad::member_object_pointing t>
member_object_<t>::~member_object_()
{
}

template <bad::member_object_pointing t>
member_object_<t>& member_object_<t>::operator =(member_object_ rhs) noexcept
{
	member_object::operator =(std::move(rhs));

	ptr = rhs.ptr;
	getter = std::move(rhs.getter);
	setter = std::move(rhs.setter);

	rhs.ptr = nullptr;

	return *this;
}

template <bad::member_object_pointing t>
bad::type member_object_<t>::get_type() const noexcept
{
	return bad::get_type<value_type>();
}

template <bad::member_object_pointing t>
std::any member_object_<t>::get_value(const void* const instance) const noexcept
{
	return std::make_any<value_type>(get_value(*static_cast<const class_type*>(instance)));
}

template <bad::member_object_pointing t>
void member_object_<t>::set_value(void* const instance, const std::any& value) const noexcept
{
	set_value(*static_cast<class_type*>(instance), std::any_cast<value_type>(value));
}

template <bad::member_object_pointing t>
inline t member_object_<t>::get_ptr() const noexcept
{
	return ptr;
}

template <bad::member_object_pointing t>
inline member_object_<t>::value_type member_object_<t>::get_value(const class_type& instance) const noexcept
{
	return getter(instance);
}

template <bad::member_object_pointing t>
inline void member_object_<t>::set_value(class_type& instance, const value_type& value) const noexcept
{
	setter(instance, value);
}

template <bad::member_object_pointing t>
template <typename... ts>
inline member_object_<t>::value_type member_object_<t>::make_value(ts&&... args) const noexcept
{
	return value_type{std::forward<ts>(args)...};
}

}// namespace nir
