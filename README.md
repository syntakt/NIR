nir
===
> *Non-Intrusive Reflection*

This library consists of static and dynamic reflection facilities that can be applied to arbitrary classes without intrusion.

---

Features
---
* __not__ an intrusive solution: no inheritance, friending or macros within classes are involved in the registration process, leaving your project clean,
* only a single outside statement is necessary to specify class meta type information,
* reflect (__even private__) base classes, member functions and member objects without any loss of type information,
* uses modern language features from the C++ 20 standard (module structure planned in version 1.0),
* no 3rd party dependencies or external tooling,
* multiplatform development with GCC and MSVC.

Usage
---
Register:
```cpp
#include <nir/reflector.h>

class my_class : public my_class_base {
	public:
	my_class(const int x, const int y) noexcept;

	protected:
	void set_x(const int x) noexcept;

	void set_y(const int y) noexcept;

	private:
	int x,
	    y;
};

NIR_REFLECT(my_class)<nir::usr::base<my_class_base>{},
                      nir::usr::member_function{"set_x", &my_class::set_x},
                      nir::usr::member_function{"set_y", &my_class::set_y},
                      nir::usr::member_object{"x", &my_class::x},
                      nir::usr::member_object{"y", &my_class::y}>;
```

Instantiate meta type:
```cpp
#include <nir/reflection_.h>

nir::reflection_<my_class> my_class_reflection;
nir::reflection& reflection = my_class_reflection;
```

Query members:
```cpp
const nir::member_object& x_member_object = *reflection.get_member_object("x");
const nir::member_function& set_x_member_function = *reflection.get_member_function("set_x");

const auto& int_x_member_object = static_cast<const nir::member_object_<int my_class::*>&>(x_member_object);
const auto& int_set_x_member_function = static_cast<const nir::member_function_<void (my_class::*)(const int)>&>(x_member_function);
```

Acquire member names:
```cpp
const std::string_view x_member_object_name = x_member_object.get_name(),
                       set_x_member_function_name = set_x_member_function.get_name();
```

Get/set member objects and call member functions:
```cpp
my_class my_class_instance;
my_class* const my_class_instance_ptr = &my_class_instance;

const std::any x_any = x_member_object.get_value(my_class_instance_ptr);
x_member_object.set_value(my_class_instance_ptr, x_any);
set_x_member_function.call(my_class_instance_ptr, {x_any});

int x = int_x_member_object.get_value(my_class_instance);
int_x_member_object.set_value(my_class_instance, x);
int_set_x_member_function.call(my_class_instance, x);
```

Iterate over member objects and member functions:
```cpp
for (const nir::member_object* const member_object : reflection.get_member_objects()) {
	// ...
}
for (const nir::member_function* const member_function : reflection.get_member_functions()) {
	// ...
}

bad::tuple_iterator{my_class_reflection.get_member_object_tuple()}(
	[&my_class_instance] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object) noexcept
	{
		// ...
	}
);
bad::tuple_iterator{my_class_reflection.get_member_function_tuple()}(
	[&my_class_instance] <bad::member_function_pointing mfp> (const nir::member_function_<mfp>& member_function) noexcept
	{
		// ...
	}
);
```

Retrieve type information:
```cpp
const auto type_printer = [] (const bad::type type) noexcept { std::cout << type.id << " = " << type.name << '\n'; };

// my_class and its bases
type_printer(reflection.get_type());
for (const bad::type& my_class_base_type : reflection.get_base_types())
	type_printer(my_class_base_type.name);

// members
type_printer(x_member_object.get_type());
type_printer(set_x_member_function.get_return_type());
for (const bad::type param_type : set_x_member_function.get_param_types())
	type_printer(param_type);
```

Interface member objects through custom getters & setters:
```cpp
using ref_ptr_getter = decltype([] (const ref_ptr& instance) noexcept -> referenced* { return instance.get(); });
using ref_ptr_setter = decltype([] (ref_ptr& instance, referenced* const referenced) noexcept { instance.set(referenced); /* increments ref. count */ });

NIR_REFLECT(ref_ptr)<nir::usr::member_object{"referenced", nir::member_object_interface{&ref_ptr::referenced, ref_ptr_getter{}, ref_ptr_setter{}}}>;
```

TLDR, with this reflection library you will be able to:
* iterate over registered class members,
* perform metaprogramming based on the reflected class and member types,
* dynamically perform actions based on custom runtime type information,
* retrieve compile-time evaluated class and member IDs and names,
* get and set (extendable) member object values, call member functions.

Examples
---
Serializing arbitrary objects to (slightly customized) JSON:
```cpp
template <typename t>
void json_writer::write(const t& object) noexcept
{
	stream << '{';

	bad::tuple_iterator{nir::reflection_<t>{}.get_member_object_tuple()}(
		[i = 0, &object, this] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object) mutable
		{
			if (i++)
				stream << ',';

			const auto& value = member_object.get_value(object);
			stream << '\"'
			       << member_object.get_type().name
			       << ' '
			       << member_object.get_name() << "\":";
			write(value);

			return false;
		}
	);

	stream << '}';
}
```

Deserialization (simplified version without the surrounding token parsing):
```cpp
template <typename t>
void json_reader::read() noexcept
{
	t object;

	bad::tuple_iterator{nir::reflection_<t>{}.get_member_object_tuple()}(
		[&object, this] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object) noexcept
		{
			using member_object_value_type = typename bad::member_pointer_traits<mop>::value_type;

			member_object.set_value(object, read<member_object_value_type>());

			return false;
		}
	);

	return object;
}
```
[Full example](https://codeberg.org/syntakt/NIR/src/branch/master/examples/json).

Limitations
---
Due to the nature of explicit template instantiation utilized for accessing `private` and `protected` members, `NIR_REFLECT(user_type)`
must appear before any `nir::reflection_<user_type>` instantiations.

Template classes must be fully defined to reflect individual members. No partial definition is allowed.
```cpp
template <int DEFAULT_VALUE>
struct my_struct { int value = DEFAULT_VALUE; };

NIR_REFLECT(my_struct<0>)<nir::usr::member_object{"value", &my_struct<0>::value}>;
NIR_REFLECT(my_struct<1>)<nir::usr::member_object{"value", &my_struct<1>::value}>;
NIR_REFLECT(my_struct<2>)<nir::usr::member_object{"value", &my_struct<2>::value}>;
NIR_REFLECT(my_struct<3>)<nir::usr::member_object{"value", &my_struct<3>::value}>;
NIR_REFLECT(my_struct<4>)<nir::usr::member_object{"value", &my_struct<4>::value}>;
NIR_REFLECT(my_struct<5>)<nir::usr::member_object{"value", &my_struct<5>::value}>;
// ...
```
Projects with many complicated template classes are not encouraged to use this reflection system!

Compilation
---
Please check out README [here](https://codeberg.org/syntakt/urob) and follow the instructions for usage.

Credits
---
* Johannes Schaub (litb): [non-public section access trick](https://bloglitb.blogspot.com/2011/12/access-to-private-members-safer.html).

---
Copyright &copy; 2022 syntakt
