////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_object.h>

json_object::json_object() noexcept
{
}

json_object::json_object([[maybe_unused]] const json_object& src) noexcept
{
}

json_object::json_object([[maybe_unused]] json_object&& src) noexcept
{
}

json_object::~json_object()
{
}

json_object& json_object::operator =([[maybe_unused]] json_object rhs) noexcept
{
	return *this;
}

void json_object::serialize([[maybe_unused]] json_writer& json_writer) const noexcept
{
}

void json_object::deserialize([[maybe_unused]] json_reader& json_reader) noexcept
{
}
