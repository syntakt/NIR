////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_REFLECTIVES_H
#define NIR_REFLECTIVES_H

#include <bad/type/concepts.h>
#include "_detail/_member_desc.h"

namespace nir::usr
{

template <bad::structural b>
struct base final {
	using type = b;
};

template <std::size_t NAME_SIZE, bad::member_function_pointing t>
using member_function = _detail::_member_desc<NAME_SIZE, t>;

template <std::size_t NAME_SIZE, _detail::_member_object_defining t>
using member_object = _detail::_member_desc<NAME_SIZE, t>;

}// namespace nir::usr

#include "_detail/reflectives.inl"

#endif
