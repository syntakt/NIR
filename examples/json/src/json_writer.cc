////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_writer.h>

json_writer::json_writer() noexcept :
	stream{},
	extras{},
	written_objects{}
{
}

json_writer::json_writer([[maybe_unused]] const json_writer& src) noexcept :
	stream{},
	extras{},
	written_objects{}
{
}

json_writer::json_writer(json_writer&& src) noexcept :
	stream{std::move(src.stream)},
	extras{std::move(src.extras)},
	written_objects{std::move(src.written_objects)}
{
}

json_writer::~json_writer()
{
}

json_writer& json_writer::operator =(json_writer rhs) noexcept
{
	stream = std::move(rhs.stream);
	extras = std::move(rhs.extras);
	written_objects = std::move(rhs.written_objects);

	return *this;
}

std::string json_writer::operator ()(const json_object& json_object) noexcept
{
	json_object.serialize(*this);

	for (decltype(extras) extras; (extras = std::move(this->extras)).size();)
		for (const extra& extra : extras)
			extra.write(*this);

	return stream.str();
}

json_writer::extra::extra(const void* const ptr, write_func_type write_func) noexcept :
	ptr{ptr},
	write_func{write_func}
{
}

json_writer::extra::extra(const extra& src) noexcept :
	ptr{src.ptr},
	write_func{src.write_func}
{
}

json_writer::extra::extra(extra&& src) noexcept :
	ptr{src.ptr},
	write_func{src.write_func}
{
	src.ptr = nullptr;
	src.write_func = nullptr;
}

json_writer::extra::~extra()
{
}

json_writer::extra& json_writer::extra::operator =(extra rhs) noexcept
{
	ptr = rhs.ptr;
	write_func = rhs.write_func;

	rhs.ptr = nullptr;
	rhs.write_func = nullptr;

	return *this;
}

void json_writer::extra::write(json_writer& writer) const noexcept
{
	write_func(ptr, writer);
}

void json_writer::write(const char* str) noexcept
{
	stream << '\"' << str << '\"';
}

void json_writer::write(const std::string& string) noexcept
{
	write(string.c_str());
}
