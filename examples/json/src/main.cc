////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <list>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <json_object.h>
#include <json_writer.h>
#include <json_decorator.h>
#include <json_reader.h>
#include <nir/reflector.h>
#include <bad/factory.h>
#include <io/text_loader.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wmultiple-inheritance"
#endif

class class_a {
	public:
	class_a() noexcept :
		pair{},
		numbers{}
	{
	}

	class_a(const class_a& src) noexcept :
		pair{src.pair},
		numbers{src.numbers}
	{
	}

	class_a(class_a&& src) noexcept :
		pair{std::move(src.pair)},
		numbers{std::move(src.numbers)}
	{
	}

	virtual ~class_a()
	{
	}

	class_a& operator =(class_a rhs) noexcept
	{
		pair = std::move(rhs.pair);
		numbers = std::move(rhs.numbers);

		return *this;
	}

	void do_nothing() const noexcept
	{
	}

	const std::pair<int, int>& get_pair() const noexcept
	{
		return pair;
	}

	void set_pair(const std::pair<int, int>& pair) noexcept
	{
		this->pair = pair;
	}

	const std::vector<double>& get_numbers() const noexcept
	{
		return numbers;
	}

	void set_numbers(const std::vector<double>& numbers) noexcept
	{
		this->numbers = numbers;
	}

	private:
	std::pair<int, int> pair;

	std::vector<double> numbers;
};

class class_b : public class_a {
	public:
	class_b() noexcept :
		class_a{},
		fp{}
	{
	}

	class_b(const class_b& src) noexcept :
		class_a{src},
		fp{src.fp}
	{
	}

	class_b(class_b&& src) noexcept :
		class_a{std::move(src)},
		fp{src.fp}
	{
	}

	~class_b() override
	{
	}

	class_b& operator =(class_b rhs) noexcept
	{
		class_a::operator =(std::move(rhs));

		fp = rhs.fp;

		return *this;
	}

	float get_fp() const noexcept
	{
		return fp;
	}

	void set_fp(const float fp) noexcept
	{
		this->fp = fp;
	}

	private:
	float fp;
};

class class_c : public json_object {
	public:
	class_c() noexcept :
		json_object{},
		character{}
	{
	}

	class_c(const class_c& src) noexcept :
		json_object{src},
		character{src.character}
	{
	}

	class_c(class_c&& src) noexcept :
		json_object{std::move(src)},
		character{src.character}
	{
	}

	~class_c() override
	{
	}

	class_c& operator =(class_c rhs) noexcept
	{
		json_object::operator =(std::move(rhs));

		character = rhs.character;

		return *this;
	}

	void serialize(json_writer& json_writer) const noexcept override
	{
		json_writer.write_object(*this);
	}

	void deserialize(json_reader& json_reader) noexcept override
	{
		json_reader.read_object(*this);
	}

	char get_character() const noexcept
	{
		return character;
	}

	void set_character(const char character) noexcept
	{
		this->character = character;
	}

	public:
	char character;
};

class class_d final : public class_c, public class_b {
	public:
	class_d() noexcept :
		class_c{},
		class_b{},
		fp{},
		string_vectors{}
	{
	}

	class_d(const class_d& src) noexcept :
		class_c{src},
		class_b{src},
		fp{src.fp},
		string_vectors{src.string_vectors}
	{
	}

	class_d(class_d&& src) noexcept :
		class_c{std::move(src)},
		class_b{std::move(src)},
		fp{src.fp},
		string_vectors{std::move(src.string_vectors)}
	{
	}

	~class_d() override
	{
	}

	class_d& operator =(class_d rhs) noexcept
	{
		class_c::operator =(std::move(rhs));
		class_b::operator =(std::move(rhs));

		fp = rhs.fp;
		string_vectors = std::move(rhs.string_vectors);

		return *this;
	}

	void serialize(json_writer& json_writer) const noexcept override
	{
		json_writer.write_object(*this);
	}

	void deserialize(json_reader& json_reader) noexcept override
	{
		json_reader.read_object(*this);
	}

	float get_fp() const noexcept
	{
		return fp;
	}

	void set_fp(const float fp) noexcept
	{
		this->fp = fp;
	}

	const std::list<std::vector<std::string>>& get_string_vectors() const noexcept
	{
		return string_vectors;
	}

	void set_string_vectors(const std::list<std::vector<std::string>>& string_vectors) noexcept
	{
		this->string_vectors = string_vectors;
	}

	private:
	float fp;

	std::list<std::vector<std::string>> string_vectors;
};

class bar final {
	public:
	bar() noexcept :
		number{},
		d{},
		nested{},
		number_ptr{nullptr}
	{
	}

	bar(const bar& src) noexcept :
		number{src.number},
		d{src.d},
		nested{src.nested},
		number_ptr{src.number_ptr}
	{
	}

	bar(bar&& src) noexcept :
		number{src.number},
		d{std::move(src.d)},
		nested{std::move(src.nested)},
		number_ptr{src.number_ptr}
	{
		src.number_ptr = nullptr;
	}

	~bar()
	{
	}

	bar& operator =(bar rhs) noexcept
	{
		number = rhs.number;
		d = std::move(rhs.d);
		nested = std::move(rhs.nested);
		number_ptr = rhs.number_ptr;

		rhs.number_ptr = nullptr;

		return *this;
	}

	int get_number() const noexcept
	{
		return number;
	}

	void set_number(const int number) noexcept
	{
		this->number = number;
	}

	const class_d& get_d() const noexcept
	{
		return d;
	}

	void set_d(const class_d& d) noexcept
	{
		this->d = d;
	}

	double get_nested_x() const noexcept
	{
		return nested.x;
	}

	void set_nested_x(const double nested_x) noexcept
	{
		nested.x = nested_x;
	}

	int* get_number_ptr() const noexcept
	{
		return number_ptr;
	}

	void set_number_ptr(int* const number_ptr) noexcept
	{
		this->number_ptr = number_ptr;
	}

	private:
	struct nested final {
		double x;
	};

	int number;

	class_d d;

	struct nested nested;

	int* number_ptr;
};

template <typename t>
class templated final {
	public:
	templated() noexcept :
		value{},
		number_ptr{nullptr}
	{
	}

	templated(const templated& src) noexcept :
		value{src.value},
		number_ptr{src.number_ptr}
	{
	}

	templated(templated&& src) noexcept :
		value{std::move(src.value)},
		number_ptr{src.number_ptr}
	{
		src.number_ptr = nullptr;
	}

	~templated()
	{
	}

	templated& operator =(templated rhs) noexcept
	{
		value = std::move(rhs.value);
		number_ptr = rhs.number_ptr;

		rhs.number_ptr = nullptr;

		return *this;
	}

	const t& get_value() const noexcept
	{
		return value;
	}

	void set_value(const t& value) noexcept
	{
		this->value = value;
	}

	int* get_number_ptr() const noexcept
	{
		return number_ptr;
	}

	void set_number_ptr(int* const number_ptr) noexcept
	{
		this->number_ptr = number_ptr;
	}

	private:
	t value;

	int* number_ptr;
};

class ref_counter : public json_object {
	public:
	ref_counter() noexcept :
		json_object{},
		count{0}
	{
	}

	ref_counter(const ref_counter& src) noexcept :
		json_object{src},
		count{0}
	{
	}

	ref_counter(ref_counter&& src) noexcept :
		json_object{std::move(src)},
		count{0}
	{
	}

	~ref_counter() override
	{
	}

	ref_counter& operator =(ref_counter rhs) noexcept
	{
		json_object::operator =(std::move(rhs));

		return *this;
	}

	void retain() noexcept
	{
		++count;
	}

	void release() noexcept
	{
		if (count && !--count)
			delete this;
	}

	private:
	unsigned count;
};

class integer final : public ref_counter {
	public:
	integer() noexcept :
		ref_counter{},
		value{0}
	{
	}

	integer(const integer& src) noexcept :
		ref_counter{src},
		value{src.value}
	{
	}

	integer(integer&& src) noexcept :
		ref_counter{std::move(src)},
		value{src.value}
	{
		src.value = 0;
	}

	~integer() override
	{
	}

	integer& operator =(integer rhs) noexcept
	{
		ref_counter::operator =(std::move(rhs));

		value = rhs.value;

		rhs.value = 0;

		return *this;
	}

	void serialize(json_writer& json_writer) const noexcept override
	{
		json_writer.write_object(*this);
	}

	void deserialize(json_reader& json_reader) noexcept override
	{
		json_reader.read_object(*this);
	}

	int get_value() const noexcept
	{
		return value;
	}

	void set_value(const int value) noexcept
	{
		this->value = value;
	}

	private:
	int value;
};

template <std::derived_from<ref_counter> t>
class ref_ptr final {
	public:
	ref_ptr(t* const object = nullptr) noexcept :
		object{object}
	{
		if (object)
			object->retain();
	}

	ref_ptr(const ref_ptr& src) noexcept :
		object{src.object}
	{
		if (object)
			object->retain();
	}

	ref_ptr(ref_ptr&& src) noexcept :
		object{src.object}
	{
		src.object = nullptr;
	}

	~ref_ptr()
	{
		if (object)
			object->release();
	}

	ref_ptr& operator =(ref_ptr rhs) noexcept
	{
		if (object)
			object->release();

		object = rhs.object;

		rhs.object = nullptr;

		return *this;
	}

	operator t*() const noexcept
	{
		return object;
	}

	t& operator *() const noexcept
	{
		return *object;
	}

	t* operator ->() const noexcept
	{
		return object;
	}

	private:
	t* object;
};

class foo final : public json_object {
	public:
	foo() noexcept :
		json_object{},
		bar{},
		number{},
		str{},
		map{},
		c_ptr{},
		boolean{},
		templated{},
		integer_ref_ptr{}
	{
	}

	foo(const foo& src) noexcept :
		json_object{src},
		bar{src.bar},
		number{src.number},
		str{src.str},
		map{src.map},
		c_ptr{src.c_ptr},
		boolean{src.boolean},
		templated{src.templated},
		integer_ref_ptr{src.integer_ref_ptr}
	{
	}

	foo(foo&& src) noexcept :
		json_object{std::move(src)},
		bar{std::move(src.bar)},
		number{src.number},
		str{std::move(src.str)},
		map{std::move(src.map)},
		c_ptr{std::move(src.c_ptr)},
		boolean{src.boolean},
		templated{std::move(src.templated)},
		integer_ref_ptr{std::move(src.integer_ref_ptr)}
	{
	}

	~foo() override
	{
	}

	foo& operator =(foo rhs) noexcept
	{
		json_object::operator =(std::move(rhs));

		bar = std::move(rhs.bar);
		number = rhs.number;
		str = std::move(rhs.str);
		map = std::move(rhs.map);
		c_ptr = std::move(rhs.c_ptr);
		boolean = rhs.boolean;
		templated = rhs.templated;
		integer_ref_ptr = std::move(rhs.integer_ref_ptr);

		return *this;
	}

	void serialize(json_writer& json_writer) const noexcept override
	{
		json_writer.write_object(*this);
	}

	void deserialize(json_reader& json_reader) noexcept override
	{
		json_reader.read_object(*this);
	}

	const class bar& get_bar() const noexcept
	{
		return bar;
	}

	void set_bar(const class bar& bar) noexcept
	{
		this->bar = bar;
	}

	double get_number() const noexcept
	{
		return number;
	}

	void set_number(const double number) noexcept
	{
		this->number = number;
	}

	const std::string& get_str() const noexcept
	{
		return str;
	}

	void set_str(const std::string& str) noexcept
	{
		this->str = str;
	}

	const std::map<int, double>& get_map() const noexcept
	{
		return map;
	}

	void set_map(const std::map<int, double>& map) noexcept
	{
		this->map = map;
	}

	const std::shared_ptr<class_c>& get_c_ptr() const noexcept
	{
		return c_ptr;
	}

	void set_c_ptr(const std::shared_ptr<class_c>& c_ptr) noexcept
	{
		this->c_ptr = c_ptr;
	}

	bool get_boolean() const noexcept
	{
		return boolean;
	}

	void set_boolean(const bool boolean) noexcept
	{
		this->boolean = boolean;
	}

	const class templated<int>& get_templated() const noexcept
	{
		return templated;
	}

	void set_templated(const class templated<int>& templated) noexcept
	{
		this->templated = templated;
	}

	const ref_ptr<integer>& get_integer_ref_ptr() const noexcept
	{
		return integer_ref_ptr;
	}

	void set_integer_ref_ptr(const ref_ptr<integer>& integer_ref_ptr) noexcept
	{
		this->integer_ref_ptr = integer_ref_ptr;
	}

	private:
	class bar bar;

	double number;

	std::string str;

	std::map<int, double> map;

	std::shared_ptr<class_c> c_ptr;

	bool boolean;

	class templated<int> templated;

	ref_ptr<integer> integer_ref_ptr;
};

NIR_REFLECT(class_a)<nir::usr::member_function{"do_nothing", &class_a::do_nothing},
                     nir::usr::member_object{"pair", &class_a::pair},
                     nir::usr::member_object{"numbers", &class_a::numbers}>;

NIR_REFLECT(class_b)<nir::usr::base<class_a>{},
                     nir::usr::member_object{"class_b_fp", &class_b::fp}>;

NIR_REFLECT(class_c)<nir::usr::member_object{"character", &class_c::character}>;

NIR_REFLECT(class_d)<nir::usr::base<class_c>{},
                     nir::usr::base<class_b>{},
                     nir::usr::member_object{"class_d_fp", &class_d::fp},
                     nir::usr::member_object{"string_vectors", &class_d::string_vectors}>;

NIR_REFLECT(struct bar::nested)<nir::usr::member_object{"x", &bar::nested::x}>;

NIR_REFLECT(bar)<nir::usr::member_object{"number", &bar::number},
                 nir::usr::member_object{"d", &bar::d},
                 nir::usr::member_object{"nested", &bar::nested},
                 nir::usr::member_object{"number_ptr", &bar::number_ptr}>;

NIR_REFLECT(templated<int>)<nir::usr::member_object{"value", &templated<int>::value},
                            nir::usr::member_object{"number_ptr", &templated<int>::number_ptr}>;

NIR_REFLECT(integer)<nir::usr::base<ref_counter>{},
                     nir::usr::member_object{"value", &integer::value}>;

using integer_ref_ptr_getter = decltype([] (const ref_ptr<integer>& instance) noexcept -> integer* { return instance; });
using integer_ref_ptr_setter = decltype([] (ref_ptr<integer>& instance, integer* const object) noexcept { instance = object; });

NIR_REFLECT(ref_ptr<integer>)<nir::usr::member_object{"object", nir::member_object_interface{&ref_ptr<integer>::object, integer_ref_ptr_getter{}, integer_ref_ptr_setter{}}}>;

NIR_REFLECT(foo)<nir::usr::member_object{"bar", &foo::bar},
                 nir::usr::member_object{"number", &foo::number},
                 nir::usr::member_object{"str", &foo::str},
                 nir::usr::member_object{"map", &foo::map},
                 nir::usr::member_object{"c_ptr", &foo::c_ptr},
                 nir::usr::member_object{"boolean", &foo::boolean},
                 nir::usr::member_object{"templated", &foo::templated},
                 nir::usr::member_object{"integer_ref_ptr", &foo::integer_ref_ptr}>;

class foo_deversioner final : public json_reader::deversioner {
	public:
	foo_deversioner() noexcept :
		deversioner{}
	{
	}

	foo_deversioner(const foo_deversioner& src) noexcept :
		deversioner{src}
	{
	}

	foo_deversioner(foo_deversioner&& src) noexcept :
		deversioner{std::move(src)}
	{
	}

	~foo_deversioner() override
	{
	}

	foo_deversioner& operator =(foo_deversioner rhs) noexcept
	{
		deversioner::operator =(std::move(rhs));

		return *this;
	}

	void process_object(json_reader& json_reader,
	                    [[maybe_unused]] const std::string& version,
	                    void* const object,
	                    const std::vector<unprocessed_member_object>& unprocessed_member_objects) noexcept override
	{
		const nir::reflection_<foo> foo_reflection;
		for (const unprocessed_member_object& unprocessed_member_object : unprocessed_member_objects)
			if (unprocessed_member_object.name == "numba") {
				const json_reader::output_member_object<nir::member_object_<double foo::*>> member_object{
					*static_cast<const nir::member_object_<double foo::*>*>(foo_reflection.get_member_object("number")), json_reader
				};
				double number;
				json_reader.read_object(number, &unprocessed_member_object.input_range);
				member_object.set_value(*static_cast<foo*>(object), number);
			}
	}
};

int main()
{
	int x = 58;
	foo f;
	{
		bar bar;
		bar.set_number(10);
		{
			class_d d;
			d.set_pair({8, 8});
			d.set_numbers({.8});
			d.set_character('x');
			d.class_b::set_fp(.987f);
			d.set_fp(1214.f);
			d.set_string_vectors({{"abc"}, {"def", "ghi"}});
			bar.set_d(d);
		}
		bar.set_nested_x(123.321);
		bar.set_number_ptr(&x);
		f.set_bar(bar);
	}
	f.set_number(111.);
	f.set_str("abracadabra");
	f.set_map({{5, 12.}, {8, 22.}});
	{
		const std::shared_ptr<class_d> d = std::make_shared<class_d>();
		d->set_character('K');
		d->class_b::set_fp(.999f);
		d->set_fp(8);
		d->set_numbers({14.});
		d->set_string_vectors({{"magic is real"}});
		f.set_c_ptr(d);
	}
	f.set_boolean(true);
	{
		templated<int> templated;
		templated.set_value(10000);
		templated.set_number_ptr(&x);
		f.set_templated(templated);
	}
	{
		const ref_ptr<integer> integer_ref_ptr{new integer};
		integer_ref_ptr->set_value(555);
		f.set_integer_ref_ptr(integer_ref_ptr);
	}

	json_writer json_writer;
	json_writer.write_object(x);
	{
		const std::string decorated_output = json_decorator{}(json_writer(f));
		std::ofstream{"output.json", std::ios_base::out} << decorated_output;
	}

	bad::factory<json_object*> factory;
	factory.register_creator(bad::str::convert_view(bad::name_type<foo>()), [] { return new foo; });
	factory.register_creator(bad::str::convert_view(bad::name_type<class_d>()), [] { return new class_d; });
	factory.register_creator(bad::str::convert_view(bad::name_type<integer>()), [] { return new integer; });

	const std::string output_json_content = io::text_loader{"output.json"}();
	json_reader json_reader{output_json_content.c_str()};
	json_reader.set_factory(&factory);
	foo_deversioner foo_deversioner;
	json_reader.add_deversioner(foo_deversioner);
	const std::unique_ptr<const foo> ff{static_cast<foo*>(json_reader()[0])};

	const bar& f_bar = f.get_bar(),
	         & ff_bar = ff->get_bar();
	const class_d& f_d = f_bar.get_d(),
	             & ff_d = ff_bar.get_d();
	const class_d* const f_c_as_d = static_cast<const class_d*>(f.get_c_ptr().get()),
	             * const ff_c_as_d = static_cast<const class_d*>(ff->get_c_ptr().get());
	const templated<int>& f_templated = f.get_templated(),
	                    & ff_templated = ff->get_templated();

	const bool result = f_bar.get_number() == ff_bar.get_number() &&
	                    f_d.get_pair() == ff_d.get_pair() &&
	                    f_d.get_numbers() == ff_d.get_numbers() &&
	                    f_d.get_character() == ff_d.get_character() &&
	                    f_d.class_b::get_fp() == ff_d.class_b::get_fp() &&
	                    f_d.get_fp() == ff_d.get_fp() &&
	                    f_d.get_string_vectors() == ff_d.get_string_vectors() &&
	                    f_bar.get_nested_x() == ff_bar.get_nested_x() &&
	                    *f_bar.get_number_ptr() == *ff_bar.get_number_ptr() &&
	                    f.get_number() == ff->get_number() &&
	                    f.get_str() == ff->get_str() &&
	                    f.get_map() == ff->get_map() &&
	                    f_c_as_d->get_character() == ff_c_as_d->get_character() &&
	                    f_c_as_d->class_b::get_fp() == ff_c_as_d->class_b::get_fp() &&
	                    f_c_as_d->get_fp() == ff_c_as_d->get_fp() &&
	                    f_c_as_d->get_string_vectors() == ff_c_as_d->get_string_vectors() &&
	                    f.get_boolean() == ff->get_boolean() &&
	                    f_templated.get_value() == ff_templated.get_value() &&
	                    *f_templated.get_number_ptr() == *ff_templated.get_number_ptr() &&
	                    f.get_integer_ref_ptr()->get_value() == ff->get_integer_ref_ptr()->get_value();

	delete ff_bar.get_number_ptr();

	return !result;
}
