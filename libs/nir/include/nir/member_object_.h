////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_OBJECT__H
#define NIR_MEMBER_OBJECT__H

#include <bad/type/type_ops.h>
#include "member_object.h"
#include "member_object_interface.h"
#include <functional>

namespace nir
{

/// \brief Generic member object pointer wrapper implementation.
template <bad::member_object_pointing t>
class member_object_ : public member_object {
	using t_member_pointer_traits = bad::member_pointer_traits<t>;

	public:
	using value_type = t_member_pointer_traits::value_type;

	using class_type = t_member_pointer_traits::class_type;

	member_object_(const std::string_view& name, const t ptr) noexcept;

	template <bad::invocable_r<typename member_object_<t>::value_type, const typename member_object_<t>::class_type&> g,
	          std::invocable<typename member_object_<t>::class_type&, const typename member_object_<t>::value_type&> s>
	member_object_(const std::string_view& name, const member_object_interface<t, g, s>& interface) noexcept;

	member_object_(const member_object_& src) noexcept;

	member_object_(member_object_&& src) noexcept;

	~member_object_() override;

	member_object_& operator =(member_object_ rhs) noexcept;

	bad::type get_type() const noexcept override;

	std::any get_value(const void* const instance) const noexcept override;

	void set_value(void* const instance, const std::any& value) const noexcept override;

	t get_ptr() const noexcept;

	virtual value_type get_value(const class_type& instance) const noexcept;

	virtual void set_value(class_type& instance, const value_type& value) const noexcept;

	template <typename... ts>
	value_type make_value(ts&&... args) const noexcept;

	private:
	t ptr;

	std::function<value_type (const class_type& instance)> getter;

	std::function<void (class_type& instance, const value_type& value)> setter;
};

}// namespace nir

#include "_detail/member_object_.inl"

#endif
