////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_decorator.h>
#include <json_lexer.h>
#include <vector>
#include <bad/string/indent.h>

json_decorator::json_decorator() noexcept
{
}

json_decorator::json_decorator([[maybe_unused]] const json_decorator& src) noexcept
{
}

json_decorator::json_decorator([[maybe_unused]] json_decorator&& src) noexcept
{
}

json_decorator::~json_decorator()
{
}

json_decorator& json_decorator::operator =([[maybe_unused]] json_decorator rhs) noexcept
{
	return *this;
}

std::string json_decorator::operator ()(std::string input) noexcept
{
	struct decoration final {
		std::size_t indent,
		            newline_pos;
	};

	std::vector<decoration> decorations;
	const char* const input_str_begin = input.c_str();
	json_lexer lexer{input_str_begin};
	std::size_t indent = 0,
	            nested = 0;
	for (std::string token; (token = lexer.next_token()) != "";) {
		if (!nested) {
			// decrement b/c the input pointer has already stepped over the token
			const char* const lexer_input = lexer.get_input() - 1;
			if (token == "{") {
				++indent;
				decorations.emplace_back(indent, static_cast<std::size_t>(lexer_input - input_str_begin));
			}
			else if (token == "}") {
				--indent;
				decorations.emplace_back(indent, static_cast<std::size_t>(lexer_input - input_str_begin - 1));
			}
			else if (token == ",")
				decorations.emplace_back(indent, static_cast<std::size_t>(lexer_input - input_str_begin));
		}

		if (token == "[")
			++nested;
		else if (token == "]")
			--nested;
	}

	for (std::size_t extra = 0; const decoration& decoration : decorations) {
		const std::string decoration_string = "\n" + bad::indent{decoration.indent}.get();
		input.insert(decoration.newline_pos + extra + 1, decoration_string);
		extra += decoration_string.size();
	}

	return input;
}
