////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/container/tuple_iterator.h>
#include <bad/error.h>
#include <bad/string/string_ops.h>
#include <bad/type/type_ops.h>
#include <nir/reflection_.h>
#include <sstream>

inline const bad::flat_set<json_reader::deversioner*>& json_reader::get_deversioners() const noexcept
{
	return deversioners;
}

template <std::derived_from<nir::member_object> t>
json_reader::output_member_object<t>::output_member_object(const t& member_object, const json_reader& reader) noexcept :
	t{member_object},
	reader{&reader}
{
}

template <std::derived_from<nir::member_object> t>
json_reader::output_member_object<t>::output_member_object(const output_member_object& src) noexcept :
	t{src},
	reader{src.reader}
{
}

template <std::derived_from<nir::member_object> t>
json_reader::output_member_object<t>::output_member_object(output_member_object&& src) noexcept :
	t{std::move(src)},
	reader{src.reader}
{
	src.reader = nullptr;
}

template <std::derived_from<nir::member_object> t>
json_reader::output_member_object<t>::~output_member_object()
{
}

template <std::derived_from<nir::member_object> t>
json_reader::output_member_object<t>& json_reader::output_member_object<t>::operator =(output_member_object rhs) noexcept
{
	t::operator =(std::move(rhs));

	reader = rhs.reader;

	rhs.reader = nullptr;

	return *this;
}

template <std::derived_from<nir::member_object> t>
void json_reader::output_member_object<t>::set_value(class_type& instance, const value_type& value) const noexcept
{
	t::set_value(instance, value);
	reader->update_missing_ptrs(&value, &(instance.*t::get_ptr()));
}

template <typename t>
void json_reader::read_object(t& object, const input_range* const input_range) noexcept
{
	const char* const prev_input = lexer.get_input();
	if (input_range)
		lexer.set_input(input_range->start);
	output<t> output = read<t>();
	if (input_range)
		lexer.set_input(prev_input);
	object = output;
	update_missing_ptrs<t>(&output.get(), &object);
}

inline std::uintptr_t json_reader::missing_ptr::get_address() const noexcept
{
	return address;
}

inline void* json_reader::missing_ptr::get_object() const noexcept
{
	return object;
}

inline void json_reader::missing_ptr::set_object(void* const object) noexcept
{
	this->object = object;
}

template <bad::type_variadic<json_reader::output_member_object> t>
json_reader::missing_ptr_<t>::missing_ptr_(const std::uintptr_t address, void* const object, const t& member_object) noexcept :
	missing_ptr{address, object},
	member_object{member_object}
{
}

template <bad::type_variadic<json_reader::output_member_object> t>
json_reader::missing_ptr_<t>::missing_ptr_(const missing_ptr_& src) noexcept :
	missing_ptr{src},
	member_object{src.member_object}
{
}

template <bad::type_variadic<json_reader::output_member_object> t>
json_reader::missing_ptr_<t>::missing_ptr_(missing_ptr_&& src) noexcept :
	missing_ptr{std::move(src)},
	member_object{std::move(src.member_object)}
{
}

template <bad::type_variadic<json_reader::output_member_object> t>
json_reader::missing_ptr_<t>::~missing_ptr_()
{
}

template <bad::type_variadic<json_reader::output_member_object> t>
json_reader::missing_ptr_<t>& json_reader::missing_ptr_<t>::operator =(missing_ptr_ rhs) noexcept
{
	missing_ptr::operator =(std::move(rhs));

	member_object = std::move(rhs.member_object);

	return *this;
}

template <bad::type_variadic<json_reader::output_member_object> t>
void* json_reader::missing_ptr_<t>::read(json_reader& reader) const noexcept
{
	auto* const object{new std::remove_pointer_t<typename t::value_type>};
	reader.read_object(*object);
	return object;
}

template <bad::type_variadic<json_reader::output_member_object> t>
void json_reader::missing_ptr_<t>::assign(void* const ptr) const noexcept
{
	using output_member_object_value_type = t::value_type;
	auto& instance = *static_cast<t::class_type*>(get_object());
	if constexpr (bad::smart_pointing<output_member_object_value_type>)
		member_object.set_value(instance, output_member_object_value_type{static_cast<output_member_object_value_type::element_type*>(ptr)});
	else
		member_object.set_value(instance, static_cast<output_member_object_value_type>(ptr));
}

template <typename t>
json_reader::output<t>::output(json_reader& reader, const t& object) noexcept :
	reader{&reader},
	object{object}
{
}

template <typename t>
json_reader::output<t>::output(const output& src) noexcept :
	reader{src.reader},
	object{src.object}
{
}

template <typename t>
json_reader::output<t>::output(output&& src) noexcept :
	reader{src.reader},
	object{std::move(src.object)}
{
	src.reader = nullptr;

	reader->update_missing_ptrs(&src.object, &object);
}

template <typename t>
json_reader::output<t>::~output()
{
}

template <typename t>
json_reader::output<t>& json_reader::output<t>::operator =(output rhs) noexcept
{
	reader = rhs.reader;
	object = std::move(rhs.object);

	rhs.reader = nullptr;

	reader->update_missing_ptrs(&rhs.object, &object);

	return *this;
}

template <typename t>
inline json_reader::output<t>::operator const t&() const noexcept
{
	return object;
}

template <typename t>
inline t& json_reader::output<t>::get() noexcept
{
	return object;
}

template <typename t>
json_reader::output<t> json_reader::read()
{
	if (lexer.next_token() != "{")
		throw BAD_ERROR("not an object");

	enum class state {
		NAME,
		COLON,
		COMMA
	} state = state::NAME;

	output<t> output{*this};
	const nir::reflection_<t> reflection;
	const auto& reflection_member_object_tuple = reflection.get_member_object_tuple();
	std::vector<deversioner::unprocessed_member_object> unprocessed_member_objects;
	std::string version = bad::str::convert_view(reflection.get_type().name);
	member_id_parse member_id_parse;
	std::size_t i = 0;
	for (std::string token; (token = lexer.next_token()) != "" && token != "}";)
		switch (state) {
			case state::NAME:
			if (token.front() == '"' && token.back() == '"') {
				member_id_parse = parse_member_id(token);
				version += (i++ ? ", " : ": ") + bad::str::convert_view(member_id_parse.type_name) + ' ' + bad::str::convert_view(member_id_parse.name);
				state = state::COLON;
			}
			else
				throw BAD_ERROR("object parsing failed");
			break;

			case state::COLON:
			if (token == ":") {
				bool unprocessed = true;
				bad::tuple_iterator{reflection_member_object_tuple}(
					[this, member_id_parse, &output, &unprocessed] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object)
					{
						const bool matched = member_object.get_type().name == member_id_parse.type_name && member_object.get_name() == member_id_parse.name;

						if (matched) {
							using member_object_type = typename bad::nude<decltype(member_object)>::type;
							using output_member_object_type = output_member_object<member_object_type>;
							using member_object_value_type = member_object_type::value_type;

							output_member_object_type output_member_object{member_object, *this};
							try {
								output_member_object.set_value(output.get(), read<member_object_value_type>());
							}
							catch (const std::uintptr_t missing_ptr_address) {
								if constexpr (bad::pointing<member_object_value_type> || bad::smart_pointing<member_object_value_type>)
									missing_ptrs.insert(std::make_unique<missing_ptr_<output_member_object_type>>(missing_ptr_address,
									                                                                              &output.get(),
									                                                                              output_member_object));
							}
							unprocessed = false;
						}

						return matched;
					}
				);
				if (unprocessed) {
					const char* const unprocessed_member_object_input_start = lexer.get_input();
					skip_unreadable_object();
					unprocessed_member_objects.emplace_back(std::string{member_id_parse.type_name},
					                                        std::string{member_id_parse.name},
					                                        input_range{unprocessed_member_object_input_start, lexer.get_input()});
					state = state::NAME;
				}
				else
					state = state::COMMA;
			}
			else
				throw BAD_ERROR("object parsing failed");
			break;

			case state::COMMA:
			if (token == ",")
				state = state::NAME;
			else
				throw BAD_ERROR("object parsing failed");
			break;

			default:
			;
		}

	const char* const lexer_input = lexer.get_input();
	if (unprocessed_member_objects.size())
		for (deversioner* const deversioner : deversioners)
			deversioner->process_object(*this, version, &output.get(), unprocessed_member_objects);
	lexer.set_input(lexer_input);

	return output;
}

template <bad::arithmetic t>
json_reader::output<t> json_reader::read() noexcept
{
	t value;

	std::stringstream{lexer.next_token()} >> value;

	return {*this, value};
}

template <bad::pointing t>
json_reader::output<t> json_reader::read()
{
	t ptr{nullptr};

	if (const std::string token = lexer.next_token(); token != "null") {
		const std::uintptr_t address = std::stoull(token, nullptr, 16);
		if (const bad::flat_set<seen_object>::iterator it = seen_objects.find({address, nullptr});
		    it != seen_objects.end())
			ptr = static_cast<t>(it->current);
		else
			throw address;
	}

	return {*this, ptr};
}

template <bad::smart_pointing t>
json_reader::output<t> json_reader::read()
{
	return {*this, t{read<typename t::element_type*>().get()}};
}

template <std::ranges::range t>
json_reader::output<t> json_reader::read()
{
	t container;

	if (lexer.next_token() != "[")
		throw BAD_ERROR("not a collection");

	if (lexer.next_token(json_lexer::read_mode::PEEK) != "]") {
		const auto value_inserter = [this, &container] {
			container.insert(container.end(), read<typename std::remove_reference_t<t>::value_type>());
		};
		value_inserter();
		for (std::string token; (token = lexer.next_token()) != "" && token != "]";)
			if (token == ",")
				value_inserter();
	}
	else
		lexer.next_token();

	return {*this, container};
}

template <bad::type_variadic<std::pair> t>
json_reader::output<t> json_reader::read()
{
	bool ok = lexer.next_token() == "{" && lexer.next_token() == "\"first\"" && lexer.next_token() == ":";
	t pair{read<std::remove_const_t<typename t::first_type>>(), {}};
	ok &= lexer.next_token() == "," && lexer.next_token() == "\"second\"" && lexer.next_token() == ":";
	pair.second = read<typename t::second_type>();
	ok &= lexer.next_token() == "}";
	if (!ok)
		throw BAD_ERROR("not a pair");

	return {*this, pair};
}

template <typename t>
void json_reader::update_missing_ptrs(const t* const from, t* const to) const noexcept
{
	for (const std::unique_ptr<missing_ptr>& missing_ptr : missing_ptrs)
		if (missing_ptr->get_object() == from)
			missing_ptr->set_object(to);

	if constexpr (bad::structural<t>) {
		const nir::reflection_<t> reflection;
		bad::tuple_iterator{reflection.get_member_object_tuple()}(
			[this, from, to] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object)
			{
				using member_object_type = typename bad::nude<decltype(member_object)>::type;
				auto member_object_ptr = member_object.get_ptr();
				update_missing_ptrs<typename member_object_type::value_type>(&(from->*member_object_ptr), &(to->*member_object_ptr));
				return false;
			}
		);
	}
}

template <>
inline json_reader::output<std::string> json_reader::read()
{
	const std::string token = lexer.next_token();
	const std::size_t token_size = token.size();
	if (token[0] != '\"' || token[token_size - 1] != '\"')
		throw BAD_ERROR("not a string");

	return {*this, {token_size > 2 ? token.substr(1, token_size - 2) : ""}};
}
