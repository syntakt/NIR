////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

namespace nir
{

template <bad::structural t>
inline constexpr auto reflector<t>::create_base_type_list() noexcept
{
	using this_reflector = reflector<t>;
	if constexpr (requires (const this_reflector& reflector) { nir_get_bases(reflector); }) {
		const bad::type_list base_type_list = nir_get_bases(this_reflector{});
		return cat_base_type_lists<decltype(base_type_list)>() + base_type_list;
	}
	else
		return bad::type_list<>{};
}

template <bad::structural t>
constexpr auto reflector<t>::create_member_function_tuple() noexcept
{
	using this_reflector = reflector<t>;
	const std::tuple bases_member_function_tuple = [] () noexcept
	{
		if constexpr (requires (const this_reflector& reflector) { nir_get_bases(reflector); })
			return cat_base_member_function_tuples<decltype(nir_get_bases(this_reflector{}))>();
		else
			return std::make_tuple();
	}();
	if constexpr (requires (const this_reflector& reflector) { nir_get_member_functions(reflector); })
		return std::tuple_cat(bases_member_function_tuple, nir_get_member_functions(this_reflector{}));
	else
		return bases_member_function_tuple;
}

template <bad::structural t>
constexpr auto reflector<t>::create_member_object_tuple() noexcept
{
	using this_reflector = reflector<t>;
	const std::tuple bases_member_object_tuple = [] () noexcept
	{
		if constexpr (requires (const this_reflector& reflector) { nir_get_bases(reflector); })
			return cat_base_member_object_tuples<decltype(nir_get_bases(this_reflector{}))>();
		else
			return std::make_tuple();
	}();
	if constexpr (requires (const this_reflector& reflector) { nir_get_member_objects(reflector); })
		return std::tuple_cat(bases_member_object_tuple, nir_get_member_objects(this_reflector{}));
	else
		return bases_member_object_tuple;
}

template <bad::structural t>
template <bad::type_variadic<bad::type_list> tl>
constexpr auto reflector<t>::cat_base_type_lists() noexcept
{
	if constexpr (requires { typename tl::head; typename tl::tail; })
		return cat_base_type_lists<typename tl::tail>() + reflector<typename tl::head>::create_base_type_list();
	else
		return bad::type_list<>{};
}

template <bad::structural t>
template <bad::type_variadic<bad::type_list> tl>
constexpr auto reflector<t>::cat_base_member_function_tuples() noexcept
{
	if constexpr (requires { typename tl::head; typename tl::tail; })
		return std::tuple_cat(cat_base_member_function_tuples<typename tl::tail>(), reflector<typename tl::head>::create_member_function_tuple());
	else
		return std::make_tuple();
}

template <bad::structural t>
template <bad::type_variadic<bad::type_list> tl>
constexpr auto reflector<t>::cat_base_member_object_tuples() noexcept
{
	if constexpr (requires { typename tl::head; typename tl::tail; })
		return std::tuple_cat(cat_base_member_object_tuples<typename tl::tail>(), reflector<typename tl::head>::create_member_object_tuple());
	else
		return std::make_tuple();
}

}// namespace nir
