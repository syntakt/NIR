////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_REFLECTION_H
#define NIR_REFLECTION_H

#include <bad/type/type.h>
#include <vector>
#include <bad/container/flat_set.h>
#include <string_view>
#include <bad/type/concepts.h>
#include <tuple>

namespace nir
{

class member_function;
class member_object;
class member;

/// \brief Reflection generalization.
///
/// This class serves as a base for the nir::reflection_ template class. It provides the ability to work with individual reflection types
/// in a more abstract manner using polymorphic member pointer wrapper, as well as functions to query type information of the underlying
/// reflection.
class reflection {
	public:
	reflection(const reflection& src) noexcept;

	reflection(reflection&& src) noexcept;

	virtual ~reflection();

	reflection& operator =(reflection rhs) noexcept;

	constexpr bad::type get_type() const noexcept;

	constexpr const std::vector<bad::type>& get_base_types() const noexcept;

	const bad::flat_set<const member_function*>& get_member_functions() const noexcept;

	const bad::flat_set<const member_object*>& get_member_objects() const noexcept;

	const member_function* get_member_function(const std::string_view& member_function_name) const noexcept;

	const member_object* get_member_object(const std::string_view& member_object_name) const noexcept;

	protected:
	reflection(bad::type&& type, std::vector<bad::type>&& base_types) noexcept;

	void populate_member_functions(bad::type_variadic<std::tuple> auto& tuple) noexcept;

	void populate_member_objects(bad::type_variadic<std::tuple> auto& tuple) noexcept;

	private:
	template <std::derived_from<member> t>
	static void populate(bad::type_variadic<std::tuple> auto& tuple, bad::flat_set<const t*>& members) noexcept;

	bad::type type;

	std::vector<bad::type> base_types;

	bad::flat_set<const member_function*> member_functions;

	bad::flat_set<const member_object*> member_objects;
};

}// namespace nir

#include "_detail/reflection.inl"

#endif
