////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_OBJECT_H
#define NIR_MEMBER_OBJECT_H

#include "member.h"
#include <bad/type/type.h>
#include <any>

namespace nir
{

/// \brief Member object pointer wrapper.
class member_object : public member {
	public:
	member_object(const std::string_view& name = "") noexcept;

	member_object(const member_object& src) noexcept;

	member_object(member_object&& src) noexcept;

	~member_object() override;

	member_object& operator =(member_object rhs) noexcept;

	virtual bad::type get_type() const noexcept;

	virtual std::any get_value(const void* const instance) const noexcept;

	virtual void set_value(void* const instance, const std::any& value) const noexcept;
};

}// namespace nir

#endif
