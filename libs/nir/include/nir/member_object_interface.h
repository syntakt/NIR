////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_OBJECT_INTERFACE_H
#define NIR_MEMBER_OBJECT_INTERFACE_H

#include <bad/type/type_ops.h>

namespace nir
{

template <bad::member_object_pointing t,
          bad::invocable_r<typename bad::member_pointer_traits<t>::value_type, const typename bad::member_pointer_traits<t>::class_type&> g,
          std::invocable<typename bad::member_pointer_traits<t>::class_type&, const typename bad::member_pointer_traits<t>::value_type&> s>
struct member_object_interface final {
	using ptr_type = t;

	using getter_type = g;

	using setter_type = s;

	ptr_type ptr;

	getter_type getter;

	setter_type setter;
};

}// namespace nir

#endif
