////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <nir/reflection.h>
#include <utility>
#include <nir/member_function.h>
#include <nir/member_object.h>

namespace
{

template <std::derived_from<nir::member> t>
const nir::member* get_member(const std::string_view& member_name, const bad::flat_set<const t*>& members) noexcept
{
	const nir::member* member = nullptr;

	for (const nir::member* const m : members)
	{
		if (m->get_name() == member_name)
		{
			member = m;
			break;
		}
	}

	return member;
}

}// namespace

namespace nir
{

reflection::reflection(const reflection& src) noexcept :
	type{src.type},
	base_types{src.base_types},
	member_functions{src.member_functions},
	member_objects{src.member_objects}
{
}

reflection::reflection(reflection&& src) noexcept :
	type{std::move(src.type)},
	base_types{std::move(src.base_types)},
	member_functions{std::move(src.member_functions)},
	member_objects{std::move(src.member_objects)}
{
}

reflection::~reflection()
{
}

reflection& reflection::operator =(reflection rhs) noexcept
{
	type = std::move(rhs.type);
	base_types = std::move(rhs.base_types);
	member_functions = std::move(rhs.member_functions);
	member_objects = std::move(rhs.member_objects);

	return *this;
}

const member_function* reflection::get_member_function(const std::string_view& member_function_name) const noexcept
{
	return static_cast<const member_function*>(get_member(member_function_name, member_functions));
}

const member_object* reflection::get_member_object(const std::string_view& member_object_name) const noexcept
{
	return static_cast<const member_object*>(get_member(member_object_name, member_objects));
}

reflection::reflection(bad::type&& type, std::vector<bad::type>&& base_types) noexcept :
	type{std::move(type)},
	base_types{std::move(base_types)},
	member_functions{},
	member_objects{}
{
}

}// namespace nir
