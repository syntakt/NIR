////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef JSON_DECORATOR_H
#define JSON_DECORATOR_H

#include <string>

class json_decorator {
	public:
	json_decorator() noexcept;

	json_decorator(const json_decorator& src) noexcept;

	json_decorator(json_decorator&& src) noexcept;

	~json_decorator();

	json_decorator& operator =(json_decorator rhs) noexcept;

	std::string operator ()(std::string input) noexcept;
};

#endif
