////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_FUNCTION__H
#define NIR_MEMBER_FUNCTION__H

#include "member_function.h"
#include <bad/type/type_ops.h>

namespace nir
{

/// \brief Generic member function pointer wrapper implementation.
template <bad::member_function_pointing t>
class member_function_ : public member_function {
	using t_member_pointer_traits = bad::member_pointer_traits<t>;

	public:
	using return_type = t_member_pointer_traits::return_type;

	using class_type = t_member_pointer_traits::class_type;

	using param_types = t_member_pointer_traits::param_types;

	member_function_(const std::string_view& name, const t ptr) noexcept;

	member_function_(const member_function_& src) noexcept;

	member_function_(member_function_&& src) noexcept;

	~member_function_() override;

	member_function_& operator =(member_function_ rhs) noexcept;

	bad::type get_return_type() const noexcept override;

	std::vector<bad::type> get_param_types() const noexcept override;

	std::any call(void* const instance, const std::vector<std::any>& args = {}) const noexcept override;

	template <typename... ts> requires (member_function_<t>::param_types::template contains<ts...>())
	return_type call(class_type& instance, ts&&... args) const noexcept;

	private:
	t ptr;
};

}// namespace nir

#include "_detail/member_function_.inl"

#endif
