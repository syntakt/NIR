////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef JSON_WRITER_H
#define JSON_WRITER_H

#include <string>
#include <sstream>
#include <bad/type/concepts.h>
#include <bad/container/flat_set.h>
#include <bad/container/member_comparator.h>

class json_object;

class json_writer {
	public:
	json_writer() noexcept;

	json_writer(const json_writer& src) noexcept;

	json_writer(json_writer&& src) noexcept;

	~json_writer();

	json_writer& operator =(json_writer rhs) noexcept;

	std::string operator ()(const json_object& json_object) noexcept;

	template <typename t>
	void write_object(const t& object) noexcept;

	void clear_written_objects() noexcept;

	private:
	class extra final {
		public:
		using write_func_type = void (*)(const void* const ptr, json_writer& writer) noexcept;

		extra(const void* const ptr, write_func_type write_func) noexcept;

		extra(const extra& src) noexcept;

		extra(extra&& src) noexcept;

		~extra();

		extra& operator =(extra rhs) noexcept;

		template <typename t>
		static void write(const void* const ptr, json_writer& writer) noexcept;

		const void* get_ptr() const noexcept;

		void write(json_writer& writer) const noexcept;

		private:
		const void* ptr;

		write_func_type write_func;
	};

	struct written_object final {
		const void* ptr;

		std::size_t size;
	};

	template <typename t>
	void write(const t& object) noexcept;

	void write(const bad::arithmetic auto& value) noexcept;

	void write(const bad::pointing auto& ptr) noexcept;

	void write(const std::ranges::range auto& container) noexcept;

	template <typename t, typename u>
	void write(const std::pair<t, u>& pair) noexcept;

	void write(const bad::smart_pointing auto& smart_ptr) noexcept;

	void write(const char* str) noexcept;

	void write(const std::string& string) noexcept;

	std::stringstream stream;

	bad::flat_set<extra, bad::member_comparator<&extra::get_ptr>> extras;

	bad::flat_set<written_object, bad::member_comparator<&written_object::ptr>> written_objects;
};

#include "_detail/json_writer.inl"

#endif
