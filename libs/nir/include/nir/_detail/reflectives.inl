////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <tuple>
#include <nir/member_function_.h>
#include <nir/member_object_.h>

namespace nir::_detail
{

template <std::size_t NAME_SIZE, typename t>
inline constexpr _member_desc<NAME_SIZE, t>::_member_desc(const char (&name)[NAME_SIZE], t def) noexcept :
	name{},
	def{def}
{
	std::copy_n(name, NAME_SIZE, this->name);
}

template <typename... ts>
struct _base_filter;

template <typename... ts>
struct _member_function_filter;

template <typename... ts>
struct _member_object_filter;

template <>
struct _base_filter<> final {
	static constexpr auto filter() noexcept
	{
		return bad::type_list<>{};
	}
};

template <typename head, typename... tail>
struct _base_filter<head, tail...> final {
	static constexpr auto filter() noexcept
	{
		const bad::type_list tail_base_filter_type_list = _base_filter<tail...>::filter();
		using nude_head = bad::nude<head>::type;
		if constexpr (bad::type_variadic<nude_head, usr::base>)
			return bad::type_list<typename nude_head::type>{} + tail_base_filter_type_list;
		else
			return tail_base_filter_type_list;
	}
};

template <>
struct _member_function_filter<> final {
	static constexpr auto filter() noexcept
	{
		return std::tuple<>{};
	}
};

template <typename head, typename... tail>
struct _member_function_filter<head, tail...> final {
	static constexpr auto filter(head&& h, tail&&... t) noexcept
	{
		const auto tail_member_function_filter_tuple = _member_function_filter<tail...>::filter(t...);
		using nude_head = bad::nude<head>::type;
		if constexpr (_member_descriptive<nude_head> && requires { requires bad::member_function_pointing<decltype(nude_head::def)>; })
			return std::tuple_cat(std::tuple<member_function_<decltype(h.def)>>{{h.name, h.def}}, tail_member_function_filter_tuple);
		else
			return tail_member_function_filter_tuple;
	}
};

template <>
struct _member_object_filter<> final {
	static constexpr auto filter() noexcept
	{
		return std::tuple<>{};
	}
};

template <_member_object_defining t>
inline consteval auto _get_member_object_ptr_type(t def) noexcept
{
	if constexpr (bad::member_object_pointing<t>)
		return def;
	else
		return def.ptr;
}

template <typename head, typename... tail>
struct _member_object_filter<head, tail...> final {
	static constexpr auto filter(head&& h, tail&&... t) noexcept
	{
		const auto tail_member_object_filter_tuple = _member_object_filter<tail...>::filter(t...);
		using nude_head = bad::nude<head>::type;
		if constexpr (_member_descriptive<nude_head> && requires { requires _member_object_defining<decltype(h.def)>; })
			return std::tuple_cat(std::tuple<member_object_<decltype(_get_member_object_ptr_type(h.def))>>{{h.name, h.def}}, tail_member_object_filter_tuple);
		else
			return tail_member_object_filter_tuple;
	}
};

template <typename... ts>
inline constexpr auto _filter_bases([[maybe_unused]] ts&&... args) noexcept
{
	return _base_filter<ts...>::filter();
}

template <typename... ts>
inline constexpr auto _filter_member_functions(ts&&... args) noexcept
{
	return _member_function_filter<ts...>::filter(args...);
}

template <typename... ts>
inline constexpr auto _filter_member_objects(ts&&... args) noexcept
{
	return _member_object_filter<ts...>::filter(args...);
}

}// namespace nir::_detail
