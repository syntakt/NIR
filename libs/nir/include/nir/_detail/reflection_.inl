////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>

namespace nir
{
namespace _detail
{

template <bad::type_variadic<reflection_> t>
std::vector<bad::type> _get_base_types() noexcept
{
	std::vector<bad::type> base_types;

	if constexpr (requires { typename t::base_type_list::head; })
		t::base_type_list::for_each(
			[&base_types] (const auto& type_list_element)
			{
				base_types.emplace_back(bad::get_type<typename bad::nude<decltype(type_list_element)>::type::type>());
				return false;
			}
		);

	return base_types;
}

}// namespace _detail

template <bad::structural t>
reflection_<t>::reflection_() noexcept :
	reflection{bad::get_type<t>(), _detail::_get_base_types<reflection_<t>>()},
	member_function_tuple{reflector_type::create_member_function_tuple()},
	member_object_tuple{reflector_type::create_member_object_tuple()}
{
	populate_member_functions(member_function_tuple);
	populate_member_objects(member_object_tuple);
}

template <bad::structural t>
reflection_<t>::reflection_(const reflection_& src) noexcept :
	reflection{src},
	member_function_tuple{src.member_function_tuple},
	member_object_tuple{src.member_object_tuple}
{
}

template <bad::structural t>
reflection_<t>::reflection_(reflection_&& src) noexcept :
	reflection{std::move(src)},
	member_function_tuple{std::move(src.member_function_tuple)},
	member_object_tuple{std::move(src.member_object_tuple)}
{
}

template <bad::structural t>
reflection_<t>::~reflection_()
{
}

template <bad::structural t>
reflection_<t>& reflection_<t>::operator =(reflection_ rhs) noexcept
{
	reflection::operator =(std::move(rhs));

	member_function_tuple = std::move(rhs.member_function_tuple);
	member_object_tuple = std::move(rhs.member_object_tuple);

	return *this;
}

template <bad::structural t>
inline const reflection_<t>::member_function_tuple_type& reflection_<t>::get_member_function_tuple() const noexcept
{
	return member_function_tuple;
}

template <bad::structural t>
inline const reflection_<t>::member_object_tuple_type& reflection_<t>::get_member_object_tuple() const noexcept
{
	return member_object_tuple;
}

}// namespace nir
