////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_reader.h>
#include <json_object.h>
#include <bad/factory.h>
#include <queue>

json_reader::deversioner::deversioner() noexcept
{
}

json_reader::deversioner::deversioner([[maybe_unused]] const deversioner& src) noexcept
{
}

json_reader::deversioner::deversioner([[maybe_unused]] deversioner&& src) noexcept
{
}

json_reader::deversioner::~deversioner()
{
}

json_reader::deversioner& json_reader::deversioner::operator =([[maybe_unused]] deversioner rhs) noexcept
{
	return *this;
}

void json_reader::deversioner::process_object([[maybe_unused]] json_reader& json_reader,
                                              [[maybe_unused]] const std::string& version,
                                              [[maybe_unused]] void* const object,
                                              [[maybe_unused]] const std::vector<unprocessed_member_object>& unprocessed_member_objects) noexcept
{
}

json_reader::json_reader(json_lexer&& lexer) noexcept :
	lexer{std::move(lexer)},
	factory{nullptr},
	deversioners{},
	seen_objects{},
	missing_ptrs{}
{
}

json_reader::json_reader(const json_reader& src) noexcept :
	lexer{src.lexer},
	factory{src.factory},
	deversioners{src.deversioners},
	seen_objects{src.seen_objects},
	missing_ptrs{}
{
}

json_reader::json_reader(json_reader&& src) noexcept :
	lexer{std::move(src.lexer)},
	factory{src.factory},
	deversioners{std::move(src.deversioners)},
	seen_objects{std::move(src.seen_objects)},
	missing_ptrs{std::move(src.missing_ptrs)}
{
	src.factory = nullptr;
}

json_reader::~json_reader()
{
}

json_reader& json_reader::operator =(json_reader rhs) noexcept
{
	lexer = std::move(rhs.lexer);
	factory = rhs.factory;
	deversioners = std::move(rhs.deversioners);
	seen_objects = std::move(rhs.seen_objects);
	missing_ptrs = std::move(rhs.missing_ptrs);

	rhs.factory = nullptr;

	return *this;
}

std::vector<json_object*> json_reader::operator ()() noexcept
{
	std::vector<json_object*> json_objects;

	std::queue<input_range> input_ranges;
	input_ranges.emplace(lexer.get_input(), nullptr);
	while (!input_ranges.empty()) {
		const input_range input_range = input_ranges.front();
		input_ranges.pop();
		const char* input_range_start = input_range.start,
		          * const input_range_end = input_range.end;
		lexer.set_input(input_range_start);
		for (std::string token; lexer.get_input() != input_range_end && (token = lexer.next_token()) != "";) {
			if (token != ",") {
				const std::size_t first_ws_pos = token.find_first_of(' ');
				const char* const token_ptr = token.c_str();
				const std::uintptr_t address = std::stoull(std::string{token_ptr + 2, first_ws_pos - 1}, nullptr, 16);
				const std::string_view type_name{token_ptr + first_ws_pos + 1, token.size() - first_ws_pos - 2};
				void* object = nullptr;
				if (lexer.next_token() == ":") {
					if (json_object* const json_object = static_cast<class json_object*>(object = factory->create(bad::str::convert_view(type_name))); json_object) {
						json_object->deserialize(*this);
						json_objects.emplace_back(json_object);
					}
					else if (!(object = read_non_object(address))) {
						skip_unreadable_object();
						input_ranges.emplace(input_range_start, lexer.get_input());
					}

					if (object)
						fill_missing_ptrs(*seen_objects.insert({address, object}).first);
				}
			}
			input_range_start = lexer.get_input();
		}
	}

	return json_objects;
}

bad::factory<json_object*>* json_reader::get_factory() const noexcept
{
	return factory;
}

void json_reader::set_factory(bad::factory<json_object*>* const factory) noexcept
{
	this->factory = factory;
}

void json_reader::add_deversioner(deversioner& deversioner) noexcept
{
	deversioners.insert(&deversioner);
}

void json_reader::remove_deversioner(deversioner& deversioner) noexcept
{
	deversioners.erase(&deversioner);
}

json_reader::missing_ptr::missing_ptr(const std::uintptr_t address, void* const object) noexcept :
	address{address},
	object{object}
{
}

json_reader::missing_ptr::missing_ptr(const missing_ptr& src) noexcept :
	address{src.address},
	object{src.object}
{
}

json_reader::missing_ptr::missing_ptr(missing_ptr&& src) noexcept :
	address{src.address},
	object{src.object}
{
	src.address = 0;
	src.object = nullptr;
}

json_reader::missing_ptr::~missing_ptr()
{
}

json_reader::missing_ptr& json_reader::missing_ptr::operator =(missing_ptr rhs) noexcept
{
	address = rhs.address;
	object = rhs.object;

	rhs.address = 0;
	rhs.object = nullptr;

	return *this;
}

void* json_reader::missing_ptr::read([[maybe_unused]] json_reader& reader) const noexcept
{
	return nullptr;
}

void json_reader::missing_ptr::assign([[maybe_unused]] void* const ptr) const noexcept
{
}

json_reader::member_id_parse json_reader::parse_member_id(const std::string& id) noexcept
{
	const std::size_t first_ws_pos = id.find_first_of(' ');
	std::size_t last_ws_pos = id.find_last_of(' ');
	const char* const id_c_str = id.c_str();
	return {
		std::stoull(std::string{id_c_str + 2, first_ws_pos - 1}, nullptr, 16),
		{id_c_str + first_ws_pos + 1, last_ws_pos - first_ws_pos - 1},
		{id_c_str + last_ws_pos + 1, id.size() - last_ws_pos - 2}
	};
}

void json_reader::fill_missing_ptrs(const seen_object& seen_object) noexcept
{
	for (bad::flat_set<std::unique_ptr<missing_ptr>>::iterator it = missing_ptrs.begin(), end = missing_ptrs.end(); it != end;)
		if (const std::unique_ptr<missing_ptr>& missing_ptr = *it; missing_ptr->get_address() == seen_object.previous) {
			missing_ptr->assign(seen_object.current);
			it = missing_ptrs.erase(it);
			end = missing_ptrs.end();
		}
		else
			++it;
}

void* json_reader::read_non_object(const std::uintptr_t address) noexcept
{
	void* object = nullptr;

	for (const std::unique_ptr<missing_ptr>& missing_ptr : missing_ptrs)
		if (missing_ptr->get_address() == address) {
			object = missing_ptr->read(*this);
			break;
		}

	return object;
}

void json_reader::skip_unreadable_object() noexcept
{
	std::size_t nested{0};
	for (std::string token; (token = lexer.next_token()) != "";) {
		if (!nested && token == ",")
			break;

		if (token == "{" || token == "[")
			++nested;
		else if (token == "}" || token == "]")
			--nested;
	}
}
