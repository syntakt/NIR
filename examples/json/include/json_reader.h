////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef JSON_READER_H
#define JSON_READER_H

#include <string>
#include <cstdint>
#include <utility>
#include <memory>
#include <bad/type/concepts.h>
#include <bad/container/flat_set.h>
#include <bad/container/member_comparator.h>
#include <nir/member_object.h>
#include "json_lexer.h"

namespace bad
{

template <typename t>
class factory;

}// namespace bad

class json_object;

class json_reader {
	public:
	struct input_range final {
		const char* start,
		          * end;
	};

	class deversioner {
		public:
		struct unprocessed_member_object final {
			std::string type_name,
			            name;

			struct input_range input_range;
		};

		deversioner() noexcept;

		deversioner(const deversioner& src) noexcept;

		deversioner(deversioner&& src) noexcept;

		virtual ~deversioner();

		deversioner& operator =(deversioner rhs) noexcept;

		virtual void process_object(json_reader& json_reader,
		                            const std::string& version,
		                            void* const object,
		                            const std::vector<unprocessed_member_object>& unprocessed_member_objects) noexcept;
	};

	template <std::derived_from<nir::member_object> t>
	class output_member_object final : public t {
		public:
		using typename t::class_type;

		using typename t::value_type;

		output_member_object(const t& member_object, const json_reader& reader) noexcept;

		output_member_object(const output_member_object& src) noexcept;

		output_member_object(output_member_object&& src) noexcept;

		~output_member_object() override;

		output_member_object& operator =(output_member_object rhs) noexcept;

		void set_value(class_type& instance, const value_type& value) const noexcept override;

		private:
		const json_reader* reader;
	};

	json_reader(json_lexer&& lexer) noexcept;

	json_reader(const json_reader& src) noexcept;

	json_reader(json_reader&& src) noexcept;

	~json_reader();

	json_reader& operator =(json_reader rhs) noexcept;

	std::vector<json_object*> operator ()() noexcept;

	template <typename t>
	void read_object(t& object, const input_range* const input_range = nullptr) noexcept;

	bad::factory<json_object*>* get_factory() const noexcept;

	void set_factory(bad::factory<json_object*>* const factory) noexcept;

	void add_deversioner(deversioner& deversioner) noexcept;

	const bad::flat_set<deversioner*>& get_deversioners() const noexcept;

	void remove_deversioner(deversioner& deversioner) noexcept;

	private:
	struct member_id_parse final {
		std::uintptr_t address;

		std::string_view type_name,
		                 name;
	};

	struct seen_object final {
		std::uintptr_t previous;

		void* current;
	};

	class missing_ptr {
		public:
		missing_ptr(const std::uintptr_t address, void* const object) noexcept;

		missing_ptr(const missing_ptr& src) noexcept;

		missing_ptr(missing_ptr&& src) noexcept;

		virtual ~missing_ptr();

		missing_ptr& operator =(missing_ptr rhs) noexcept;

		std::uintptr_t get_address() const noexcept;

		void* get_object() const noexcept;

		void set_object(void* const object) noexcept;

		virtual void* read(json_reader& reader) const noexcept;

		virtual void assign(void* const ptr) const noexcept;

		private:
		std::uintptr_t address;

		void* object;
	};

	template <bad::type_variadic<json_reader::output_member_object> t>
	class missing_ptr_ final : public missing_ptr {
		public:
		missing_ptr_(const std::uintptr_t address, void* const object, const t& member_object) noexcept;

		missing_ptr_(const missing_ptr_& src) noexcept;

		missing_ptr_(missing_ptr_&& src) noexcept;

		~missing_ptr_() override;

		missing_ptr_& operator =(missing_ptr_ rhs) noexcept;

		void* read(json_reader& reader) const noexcept override;

		void assign(void* const ptr) const noexcept override;

		private:
		t member_object;
	};

	template <typename t>
	class output final {
		public:
		output(json_reader& reader, const t& object = {}) noexcept;

		output(const output& src) noexcept;

		output(output&& src) noexcept;

		~output();

		output& operator =(output rhs) noexcept;

		operator const t&() const noexcept;

		t& get() noexcept;

		private:
		json_reader* reader;

		t object;
	};

	static member_id_parse parse_member_id(const std::string& id) noexcept;

	template <typename t>
	output<t> read();

	template <bad::arithmetic t>
	output<t> read() noexcept;

	template <bad::pointing t>
	output<t> read();

	template <bad::smart_pointing t>
	output<t> read();

	template <std::ranges::range t>
	output<t> read();

	template <bad::type_variadic<std::pair> t>
	output<t> read();

	void fill_missing_ptrs(const seen_object& seen_object) noexcept;

	void* read_non_object(const std::uintptr_t address) noexcept;

	void skip_unreadable_object() noexcept;

	template <typename t>
	void update_missing_ptrs(const t* const from, t* const to) const noexcept;

	json_lexer lexer;

	bad::factory<json_object*>* factory;

	bad::flat_set<deversioner*> deversioners;

	bad::flat_set<seen_object, bad::member_comparator<&seen_object::previous>> seen_objects;

	bad::flat_set<std::unique_ptr<missing_ptr>> missing_ptrs;
};

#include "_detail/json_reader.inl"

#endif
