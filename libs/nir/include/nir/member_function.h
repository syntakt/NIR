////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_FUNCTION_H
#define NIR_MEMBER_FUNCTION_H

#include "member.h"
#include <bad/type/type.h>
#include <vector>
#include <any>

namespace nir
{

/// \brief Member function pointer wrapper.
class member_function : public member {
	public:
	member_function(const std::string_view& name = "") noexcept;

	member_function(const member_function& src) noexcept;

	member_function(member_function&& src) noexcept;

	~member_function() override;

	member_function& operator =(member_function rhs) noexcept;

	virtual bad::type get_return_type() const noexcept;

	virtual std::vector<bad::type> get_param_types() const noexcept;

	virtual std::any call(void* const instance, const std::vector<std::any>& args = {}) const noexcept;
};

}// namespace nir

#endif
