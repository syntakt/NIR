////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <json_object.h>
#include <bad/container/tuple_iterator.h>
#include <nir/reflection_.h>
#include <cstdint>

template <typename t>
void json_writer::write_object(const t& object) noexcept
{
	if (written_objects.size())
		stream << ',';
	written_objects.insert({&object, sizeof(t)});
	stream << "\"@"
	       << static_cast<const void*>(&object)
	       << ' '
	       << bad::name_type<t>()
	       << "\":";
	write(object);
}

inline void json_writer::clear_written_objects() noexcept
{
	written_objects.clear();
}

template <typename t>
void json_writer::extra::write(const void* const ptr, json_writer& writer) noexcept
{
	const t* const t_ptr = static_cast<const t*>(ptr);
	if constexpr (std::derived_from<t, json_object>)
		t_ptr->serialize(writer);
	else
		writer.write_object(*t_ptr);
}

inline const void* json_writer::extra::get_ptr() const noexcept
{
	return ptr;
}

template <typename t>
void json_writer::write(const t& object) noexcept
{
	stream << '{';
	bad::tuple_iterator{nir::reflection_<t>{}.get_member_object_tuple()}(
		[i = 0, this, &object] <bad::member_object_pointing mop> (const nir::member_object_<mop>& member_object) mutable
		{
			if (i++)
				stream << ',';
			const auto& value = member_object.get_value(object);
			stream << "\"@"
			       << static_cast<const void*>(&value)
			       << ' '
			       << member_object.get_type().name
			       << ' '
			       << member_object.get_name() << "\":";
			write(value);

			return false;
		}
	);
	stream << '}';
}

inline void json_writer::write(const bad::arithmetic auto& value) noexcept
{
	stream << value;
}

void json_writer::write(const bad::pointing auto& ptr) noexcept
{
	if (ptr) {
		stream << ptr;

		bool not_written = true;
		for (const auto& [written_object_ptr, written_object_size] : written_objects)
			not_written &= ptr < written_object_ptr || static_cast<const void*>(ptr) >= static_cast<const std::uint8_t*>(written_object_ptr) + written_object_size;
		if (not_written && extras.find({ptr, nullptr}) == extras.end())
			extras.insert({ptr, &extra::write<std::remove_pointer_t<std::remove_reference_t<decltype(ptr)>>>});
	}
	else
		stream << "null";
}

void json_writer::write(const std::ranges::range auto& container) noexcept
{
	stream << '[';
	auto it = container.begin();
	if (it != container.end()) {
		write(*it++);
		for (; it != container.end(); ++it) {
			stream << ',';
			write(*it);
		}
	}
	stream << ']';
}

template <typename t, typename u>
void json_writer::write(const std::pair<t, u>& pair) noexcept
{
	stream << "{\"first\":";
	write(pair.first);
	stream << ",\"second\":";
	write(pair.second);
	stream << '}';
}

void json_writer::write(const bad::smart_pointing auto& smart_ptr) noexcept
{
	write(smart_ptr.get());
}
