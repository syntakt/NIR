////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_REFLECTION__H
#define NIR_REFLECTION__H

#include "reflection.h"
#include "reflector.h"

namespace nir
{

/// \brief Static reflection class.
///
/// Contains concrete member wrapper tuples where every wrapper consist of member pointers (having its actual type).
/// It also implements the nir::reflection interface base and type querying. Instances of #reflection_ can then be
/// used to retrieve the tuples to iterate over (for example, using bad::tuple_iterator). Please see nir::member_object_
/// and nir::member_function_ for more information on wrapper capabilities.
///
/// \code{.cpp}
/// my_class my_class_instance;
/// nir::reflection_<my_class> my_class_reflection;
/// bad::tuple_iterator{my_class_reflection.get_member_object_tuple()}(
///         [&my_class_instance] (const nir::member_object_<auto>& member_object)
///         { std::cout << member_object.get_name() << ' ' << member_object.get_value(my_class_instance); }
/// );
/// \endcode
template <bad::structural t>
class reflection_ : public reflection {
	using reflector_type = reflector<t>;

	public:
	using type = t;

	using base_type_list = decltype(reflector_type::create_base_type_list());

	using member_function_tuple_type = decltype(reflector_type::create_member_function_tuple());

	using member_object_tuple_type = decltype(reflector_type::create_member_object_tuple());

	reflection_() noexcept;

	reflection_(const reflection_& src) noexcept;

	reflection_(reflection_&& src) noexcept;

	~reflection_() override;

	reflection_& operator =(reflection_ rhs) noexcept;

	const member_function_tuple_type& get_member_function_tuple() const noexcept;

	const member_object_tuple_type& get_member_object_tuple() const noexcept;

	private:
	member_function_tuple_type member_function_tuple;

	member_object_tuple_type member_object_tuple;
};

}// namespace nir

#include "_detail/reflection_.inl"

#endif
