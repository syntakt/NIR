////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <nir/member.h>
#include <utility>

namespace nir
{

member::member(const std::string_view& name) noexcept :
	name{name}
{
}

member::member(const member& src) noexcept :
	name{src.name}
{
}

member::member(member&& src) noexcept :
	name{std::move(src.name)}
{
}

member::~member()
{
}

member& member::operator =(member rhs) noexcept
{
	name = std::move(rhs.name);

	return *this;
}

}// namespace nir
