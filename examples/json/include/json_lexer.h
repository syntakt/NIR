////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef JSON_LEXER_H
#define JSON_LEXER_H

#include <io/reader.h>
#include <string>

class json_lexer : public io::reader {
	public:
	enum class read_mode {
		STEP,
		PEEK
	};

	json_lexer(const char* const input) noexcept;

	json_lexer(const json_lexer& src) noexcept;

	json_lexer(json_lexer&& src) noexcept;

	~json_lexer();

	json_lexer& operator =(json_lexer rhs) noexcept;

	std::string next_token(const read_mode read_mode = read_mode::STEP) noexcept;
};

#endif
