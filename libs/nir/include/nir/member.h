////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef NIR_MEMBER_H
#define NIR_MEMBER_H

#include <string_view>

namespace nir
{

/// \brief Member pointer wrapper.
class member {
	public:
	member(const std::string_view& name = "") noexcept;

	member(const member& src) noexcept;

	member(member&& src) noexcept;

	virtual ~member();

	member& operator =(member rhs) noexcept;

	constexpr const std::string_view& get_name() const noexcept;

	private:
	std::string_view name;
};

}// namespace nir

#include "_detail/member.inl"

#endif
