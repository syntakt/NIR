////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type_list_expander.h>

namespace nir
{

template <bad::member_function_pointing t>
member_function_<t>::member_function_(const std::string_view& name, const t ptr) noexcept :
	member_function{name},
	ptr{ptr}
{
}

template <bad::member_function_pointing t>
member_function_<t>::member_function_(const member_function_& src) noexcept :
	member_function{src},
	ptr{src.ptr}
{
}

template <bad::member_function_pointing t>
member_function_<t>::member_function_(member_function_&& src) noexcept :
	member_function{std::move(src)},
	ptr{src.ptr}
{
	src.ptr = nullptr;
}

template <bad::member_function_pointing t>
member_function_<t>::~member_function_()
{
}

template <bad::member_function_pointing t>
member_function_<t>& member_function_<t>::operator =(member_function_ rhs) noexcept
{
	member_function::operator =(std::move(rhs));

	ptr = rhs.ptr;

	rhs.ptr = nullptr;

	return *this;
}

template <bad::member_function_pointing t>
inline bad::type member_function_<t>::get_return_type() const noexcept
{
	return bad::get_type<return_type>();
}

template <bad::member_function_pointing t>
std::vector<bad::type> member_function_<t>::get_param_types() const noexcept
{
	std::vector<bad::type> param_types;

	if constexpr (requires { typename param_types::head; })
		param_types::for_each(
			[&param_types] (const auto& element) noexcept
			{
				param_types.emplace_back(bad::get_type<typename bad::nude<decltype(element)>::type::type>());

				return false;
			}
		);

	return param_types;
}

template <bad::member_function_pointing t> 
std::any member_function_<t>::call(void* const instance, const std::vector<std::any>& args) const noexcept
{
	std::any ret;

	bad::type_list_expander<param_types>{}(
		[ptr = ptr, instance, &ret] (auto&&... args) noexcept
		{
			if constexpr (std::is_same_v<return_type, void>)
				(static_cast<class_type*>(instance)->*ptr)(args...);
			else
				ret = (static_cast<class_type*>(instance)->*ptr)(args...);
		},
		args
	);

	return ret;
}

template <bad::member_function_pointing t>
template <typename... ts> requires (member_function_<t>::param_types::template contains<ts...>())
inline member_function_<t>::return_type member_function_<t>::call(class_type& instance, ts&&... args) const noexcept
{
	return static_cast<return_type>((instance.*ptr)(std::forward<ts>(args)...));
}

}// namespace nir
